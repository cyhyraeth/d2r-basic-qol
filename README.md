## Diablo II Resurrected Basic QoL

This is a basic quality of life mod for Diablo II Resurrected

### Features

- Stack Size

  - Key stack size increased from 12 to 50

  - Tome stack size increased from 20 to 80

  - Arrow and bolt stack size increased to 500

- Town Cast

  - Enable additional spells to be cast in town

    - Teleport, Thunderstorm, Armageddon, Hurricane

    - Charge, Leap, Shout, Battle Orders, Battle Command

  - Allow some spells to be cast in Werewolf and Werebear

    - Teleport, Shout, Battle Orders, Battle Command

- Quest Bug

  - Always get quest drops from Andariel

  - Even if you forget to talk to Warriv

- Unsocket

  - New cube recipe for unsocketing

  - Scroll of Town Portal + Socketed Item

  - Unsockets items without destroying runes

- Respec

  - Free, unlimited respeccing

  - New cube recipe for respec token

  - Scroll of Town Portal + Scroll of Identify

### Installation

1. Download the most recent release from the
   [releases page](https://github.com/cyhyraethz/d2r-basic-qol/releases)

2. Open your file manager and navigate to your Diablo II Resurrected installation directory,
   usually located in `C:\Program Files (x86)\Diablo II Resurrected`

3. Create a new folder named `mods` in the Diablo II Resurrected installation directory

4. Create a new folder named `basic-qol` in the new `mods` folder

5. Copy basic-qol.mpq into the new `basic-qol` folder, which should look like this:

   `C:\Program FIles (x86)\Diablo II Resurrected\mods\basic-qol\basic-qol.mpq`

6. Create a new shortcut for D2R.exe on your Desktop

7. Right click on the new shortcut, select properties, and add the `-mod basic-qol` parameters

8. Start Diablo II Resurrected using the new shortcut and enjoy the quality of life features

### Additional Notes

If your cursor is on top of an NPC when casting Teleport in town it will register as you clicking on them
and your character will move toward them normally. You can get around this by holding down the Show Items
hotkey to prevent your character from targeting the NPC, allowing you to Teleport right next to them.

Hurricane and Armageddon can be precast in town but will not display their animations until you leave town.
